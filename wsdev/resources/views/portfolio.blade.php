@extends('layout')
@section('title')
Portfolio
@endsection
@section('menu')
     <div class="col-xl-9 col-lg-9 text-right">
        <div class="main-menu">
            <nav id="mobile-menu">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="/over-ons" >Over ons</a></li>
                    <li><a href="/services">Onze service</a></li>
                    <li><a href="/projecten" class="menu-active">Portfolio</a></li>
                    <li><a href="/contact">Contact</a></li>
                    <li><a href="/dashboard">Portaal</a></li>
                    <!-- <li><a href="http://scrum.wsdev.nl" target="blank">Mijn WSDEV</a></li> -->
                </ul>
            </nav>
        </div>
    </div>
@endsection
@section('content')
<section class="portfoilo-area pt-100 pb-70">
<div class="row">
                    <div class="col-xl-12 text-center pb-40 ">
                        <div class="section-title service-title">
                            <h2>WSDEV portfolio</h2>
                            <p>Bekijk hieronder het werk voor onze meer dan tevreden klanten, benieuwd wat wij voor u kunnen doen? Neem gerust contact met ons op! Klik op de knoppen hieronder om te sorteren op categorie</p>
                        </div>
                    </div>
                </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="portfolio-menu mb-55">
                            <button class="active" data-filter="*">Alles</button>
                            <button class="" data-filter=".cat-one">Website</button>
                            <button class="" data-filter=".cat-two">Webdesign</button>
                            <button class="" data-filter=".cat-three">Hosting</button>
                        </div>
                    </div>
                </div>

                <div class="row portfolio-active">
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-one cat-two cat-three">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb">
                                <img src="img/portfolio/worldrun.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="https://worldrun.shop" target="blank">WorldRun</a></h5>
                                <span>Website, webdesign & hosting </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-three">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb">
                                <img src="img/portfolio/entreprefleur.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="#">Entreprefleur </a></h5>
                                <span>Hosting</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-one cat-two cat-three">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb">
                                <img src="img/portfolio/schutconsultancy.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="schutconsultancy.nl" target="blank">Schut Consultancy</a></h5>
                                <span>Website, webdesign & hosting </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-four cat-one">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb">
                                <img src="img/portfolio/webmonkeys.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="https://web-monkeys.nl" target="blank">Webmonkeys</a></h5>
                                <span>Website, webdesign & hosting </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-two cat-three">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb">
                                <img src="img/portfolio/onbekend.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="schutholdig.nl" target="blank">Schut holding B.V.</a></h5>
                                <span>Webdesing & hosting </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-three">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb">
                                <img src="img/portfolio/onbekend.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="#">Care More</a></h5>
                                <span>Hosting</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item  cat-three">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb">
                                <img src="img/portfolio/onbekend.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="#">Verkiezingsspecialist.nl</a></h5>
                                <span>Hosting</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-one cat-tro cat-three">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb">
                                <img src="img/portfolio/onbekend.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="#">Direct Auto Theorie </a></h5>
                                <span>Website, webdesign & hosting</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- portfolio-cta -->
        <section class="portfolio-cta grey-bg pt-50 pb-20">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-9 mb-30">
                        <div class="call-to-action">
                            <h3>Benieuwd naar ons werkproces?</h3>
                            <p>Download dan nu onze flyer en bekijk hoe het team van WSDEV te werk gaat.</p>
                        </div>
                    </div>
                    <div class="col-md-3 mb-30">
                        <div class="call-to-action">
                            <a href="#" class="btn">download</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Related Project -->
        <section class="related-project pt-100 pb-70">
            <div class="container">
            <div class="col-xl-12 text-center pb-40 ">
                        <div class="section-title service-title">
                            <h2>Overige projecten</h2>
                            <!-- <p>Bekijk hieronder het werk voor onze meer dan tevreden klanten, benieuwd wat wij voor u kunnen doen? Neem gerust contact met ons op! Klik op de knoppen hieronder om te sorteren op categorie</p> -->
                        </div>
                    </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-two">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb">
                                <img src="img/portfolio/schoolhackje.png" class="img-fluid" alt="">
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="https://schoolhackje.nl/" target="blank">Schoolh#ckje</a></h5>
                                <span>WSDEV & Nobanovi </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-two">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb">
                                <img src="img/portfolio/gamenightmm.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="https://www.eventbrite.nl/o/wsdev-18147697829" target="blank">Game Night MM</a></h5>
                                <span>WSDEV & Emmen IT</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- clients-area -->
        <div class="clients-area grey-bg pt-50 pb-50">
            <div class="container">
                <div class="row">
                    <div class="clients-active owl-carousel">
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-1.png" alt="">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-2.png" alt="">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-3.png" alt="">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-4.png" alt="">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-05.png" alt="">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-06.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection