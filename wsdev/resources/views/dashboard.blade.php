@extends('layout')
@section('title')
Home
@endsection
@section('menu')
     <div class="col-xl-9 col-lg-9 text-right">
        <div class="main-menu">
            <nav id="mobile-menu">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="/over-ons" >Over ons</a></li>
                    <li><a href="/services">Onze service</a></li>
                    <li><a href="/projecten">Portfolio</a></li>
                    <li><a href="/contact">Contact</a></li>
                    <li><a href="/dashboard" class="menu-active">Portaal</a></li>
                    <!-- <li><a href="http://scrum.wsdev.nl" target="blank">Mijn WSDEV</a></li> -->
                </ul>
            </nav>
        </div>
    </div>
@endsection
@section('content')
        <!-- service-area -->
        <section class="service-area grey-bg pb-70 pt-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center mb-40">
                        <div class="section-title service-title">
                            <h2>WSDEV Portaal</h2>
                            <p>Welkom om op het dashboard met alle geslecteerde WSDEV-Applicaties voor u. Klik op "Bezoek" onder de applicatie om naar de applicatie te navigeren.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 text-center mb-30">
                        <div class="features-wrap">
                            <div class="features-icon">
                                <span class="lnr lnr-envelope"></span>
                            </div>
                            <h4>Webmail</h4>
                            <!-- <b> Service+ </b>  -->
                            <!-- <p>Log via deze knop direct in op uw webmail account. </p> -->
                                <a href="http://vserver275.axc.nl/roundcube/" target="_blank">Ga <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 text-center mb-30">
                        <div class="features-wrap">
                            <div class="features-icon">
                                <span class="lnr lnr-chart-bars"></span>
                            </div>
                            <h4>Project status</h4>
                            <!-- <p>Et harum quidem rerum facilis est et expe dita dis tinctio. Nam libero tempore, cum soluta -->
                                <!-- nobis..</p> -->
                                <a href="http://personeel.wsdev.nl/" target="_blank">Ga <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 text-center mb-30">
                        <div class="features-wrap">
                            <div class="features-icon">
                               <span class="lnr lnr-user"></span>
                            </div>
                            <h4>Mijn Metador</h4>
                            <!-- <p>Et harum quidem rerum facilis est et expe dita dis tinctio. Nam libero tempore, cum soluta -->
                                <!-- nobis..</p> -->
                            <a href="https://www.panel.metador.nl/login" target="_blank">Ga <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                   
                </div>
            </div>
        </section>
  
@endsection

