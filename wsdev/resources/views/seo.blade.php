@extends('layout')
@section('title')
SEO
@endsection
@section('menu')
     <div class="col-xl-9 col-lg-9 text-right">
        <div class="main-menu">
            <nav id="mobile-menu">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="/over-ons" >Over ons</a></li>
                    <li><a href="/services" class="menu-active">Onze service</a></li>
                    <li><a href="/projecten">Portfolio</a></li>
                    <li><a href="/contact">Contact</a></li>
                    <li><a href="/dashboard">Portaal</a></li>
                    <!-- <li><a href="http://scrum.wsdev.nl" target="blank">Mijn WSDEV</a></li> -->
                </ul>
            </nav>
        </div>
    </div>
@endsection
@section('content')
<!-- <div class="services-area pt-100 pb-65"> -->
      <section class="breadcrumb-area pb-70 pt-100 grey-bg" style="background-image:url(img/bg/page-title.png)">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-md-6 mb-30">
                        <div class="breadcrumb-title sm-size">
                            <h2>services </h2>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 text-left text-md-right mb-30">
                        <div class="breadcrumb">
                            <ul>
                                <li><a href="/home" style="color: white">Home</a></li>
                                <li><a href="/services" style="color: white">services</a></li>
                                <li><a href="" style="color: white">seo</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <div style="margin-top: 35px"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-4">
                        <div class="servicee-sidebar mb-50">
                            <div class="sidebar-link grey-bg">
                                <h3>Services</h3>
                                <ul>
                                    <li><a href="/webontwikkeling">Webontwikkeling</a></li>
                                    <li><a href="/webdesign">Webdesing</a></li>
                                    <li><a href="/hosting"  >Hosting</a></li>
                                    <li><a href="/contentcreation">Content Creation</a></li>
                                    <li><a href="/seo" style="color: #fa6900">SEO optimalisatie</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="servicee-sidebar mb-40">
                            <div class="sidebar-download">
                                <h3>Downloads</h3>
                                <ul>
                                    <li><a href="#"><i class="far fa-file-pdf"></i> Werkwijze.PDF</a></li>
                                    <li><a href="#"><i class="far fa-file-pdf"></i> Portfolio.PDF</a></li>
                                    <li><a href="#"><i class="far fa-file-pdf"></i> SEO.PDF</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="servicee-sidebar mb-50">
                            <a href="/contact"><img src="/img/service/details/1.png" width="100%" class="img-fluid" alt=""></a>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-8">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="service-details mb-40">
                                    <!-- <img class="mb-30" src="/img/service/details/banner-service.jpg" alt=""> -->
                                    <h3>SEO</h3>
                                    <p>Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                        mage aliquami erat volpate Ut wisi
                                        enim ad minim veniam, quis nostrud exerci tation corper suscipit lobortis nisl ut
                                        aliquip ex ea comcon sequat. Duis autem
                                        velue eum iriure dolor in hendrerit in vulputate velitesse molestie consequat .</p>
                                    <p>Fonsec tetuer adipiscing elitsed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                        mage aliquami erat volpate Ut wisi
                                        enim ad minim.</p>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="service-details mb-40">
                                            <h3>Business Analysis</h3>
                                            <p>Consectetuer adipiscing elit, sed diam nonumy nibh euismod tincidunt ut laoreet
                                                dolore mage aliquami erat volpate Ut wisi
                                                enim ad minim veniam, quis nostrud exerci tation corper cipit lobortis nisl ut
                                                aliquip ex ea comcon sequat. Duis autem velue
                                                eum iriure dolor in hendrerit in vulputate velitesse consequat .</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-6 col-lg-12">
                                        <div class="s-details-img mb-30">
                                            <img src="/img/service/details/banner-service.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-12">
                                        <div class="service-details mb-40">
                                            <h3>Awesome Teamwork Attention</h3>
                                            <p>Consectetuer adipiscing elit, sed diam nonumy nibh euismod tincidunt ut laoreet dolore mage aliquami erat volpate Ut
                                            wisi enim ad minim veniam, quis nos trud exerci tation corper cipit lobortis nisl ut aliq uip ex ea comcon sequat. Duis
                                            autem velue eum iriure dolor in hendrerit in vulputate velitesse conse quat Ut wisi enim ad minim veniam.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="service-details mb-30">
                                    <h3>Your Succes Will be Done</h3>
                                    <p>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                                        aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu fugiat nulla pariatu cepteusint occaecat
                                        cupidatat non proident, sunt in culpa qui
                                        officia deserunt mollit anim laborum. Curatur pretium tincidunt lacus. Nulla gravida
                                        orci a odio. Nullam varius, turpis et
                                        commodo pharetra est eros bibendum elit nec luctus magna felis sollicitudin mauris.
                                        Integer in mauris nibh euismod gravida.
                                        Duis ac tellus et risus vulputate vehicula.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div style="margin-bottom: : 50px"></div>
        <!-- </div> -->
    </main>

@endsection