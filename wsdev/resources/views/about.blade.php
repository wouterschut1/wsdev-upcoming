@extends('layout')
@section('title')
Over ons
@endsection
@section('menu')
     <div class="col-xl-9 col-lg-9 text-right">
        <div class="main-menu">
            <nav id="mobile-menu">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="/over-ons" class="menu-active" >Over ons</a></li>
                    <li><a href="/services">Onze service</a></li>
                    <li><a href="/projecten">Portfolio</a></li>
                    <li><a href="/contact">Contact</a></li>
                    <li><a href="/dashboard">Portaal</a></li>
                    <!-- <li><a href="http://scrum.wsdev.nl" target="blank">Mijn WSDEV</a></li> -->
                </ul>
            </nav>
        </div>
    </div>
@endsection
@section('content')

 <!-- main-start -->

<main>
        <!-- breadcrumb-area -->
        <section class="breadcrumb-area pb-70 pt-100 grey-bg" style="background-image:url(img/bg/page-title.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-md-6 mb-30">
                        <div class="breadcrumb-title">
                            <h2>Over ons</h2>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 text-left text-md-right mb-30">
                        <div class="breadcrumb">
                            <ul>
                                <li><a href="/" style="color: white">Home</a></li>
                                <li><a href="" style="color: white">Over ons</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- about-info -->
        <section class="about-info pt-100">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="about-info-text">
                            <h2>Teamwerk, dat is WSDEV!</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi tempora veritatis nemo aut ea iusto eos est expedita,quas ab adipisci. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>
                            <!-- <a href="#" class="btn">Read More</a> -->
                            <div class="info-img">
                                <img src="img/about/our-team.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- about-area -->
        <section class="about-area grey-bg pb-60 pt-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 mb-40">
                        <div class="section-title">
                            <h2>About Us</h2>
                        </div>
                        <div class="about-content">
                            <p>Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. <br> <br>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 mb-20 md-margin">
                        <div class="row">
                            <div class="col-xl-6 col-md-6 mb-20">
                                <div class="about-img">
                                    <img src="img/about/about1.jpg" class="img-fluid" alt="">
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-6 mb-20">
                                <div class="about-img">
                                    <img src="img/about/about2.jpg" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- team-are -->
        <section class="team-area pt-100 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center pb-40">
                        <div class="section-title service-title">
                            <h2>Our Team</h2>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                                alteration in some form,words which don't look even slightly believable.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-6 mb-30">
                        <div class="single-team text-center ">
                            <div class="team-img">
                                <img src="img/team/01.jpg" alt="">
                            </div>
                            <div class="team-info">
                                <h3>Tanvir Raihan</h3>
                                <span>Web Designer</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 mb-30">
                        <div class="single-team text-center ">
                            <div class="team-img">
                                <img src="img/team/02.jpg" alt="">
                            </div>
                            <div class="team-info">
                                <h3>Jackson Nash</h3>
                                <span>Massagist</span>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-xl-3 col-lg-3 col-md-6 mb-30">
                        <div class="single-team text-center ">
                            <div class="team-img">
                                <img src="img/team/03.jpg" alt="">
                            </div>
                            <div class="team-info">
                                <h3>Jon Doe</h3>
                                <span>Web Develpoer</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 mb-30">
                        <div class="single-team text-center " data-wow-delay=".8s">
                            <div class="team-img">
                                <img src="img/team/04.jpg" alt="">
                            </div>
                            <div class="team-info">
                                <h3>Jackson Nash</h3>
                                <span>Massagist</span>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>
    </main>

@endsection

