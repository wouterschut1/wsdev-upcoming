
@include('copywrite')
@include('styles')

<head>
    <title> @yield('title') - WSDEV</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#fa6900">
    <link rel="shortcut icon" href="/img/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
</head>
<header>
    <div class="header-top wsdevgrey-bg">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-6 col-md-8 text-center text-md-left">
                    <div class="header-top-cta">
                        <span><i class="far fa-envelope-open"></i><a href="mailto:info@wsdev.nl" style="color: white">info@wsdev.nl</a></span>
                        <span><i class="fas fa-phone"></i><a href="tel:06-44145340" style="color: white">06-44145340</span>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-4 text-center text-md-right">
                    <div class="top-social-links">
                        <span>Volg ons</span>
                        <a href="https://goo.gl/xNmWVH" target="blank"><i class="fab fa-instagram"></i></a>
                        <a href="https://goo.gl/PikHch" target="blank"><i class="fab fa-whatsapp"></i></a>
                        <div class="top-btn">
                            <a href="/dashboard" target="blank">WSDEV portaal</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sticky-header" class="menu-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-5">
                    <div class="logo">
                        <a href="index.html"><img src="../img/logo/logo_small.png" class="img-fluid" alt="logo" width="90%"></a>
                    </div>
                </div>
               @yield('menu')
                <div class="col-xl-1 col-lg-1 d-none d-lg-block">
                    <div class="menu-icon">
                    </div>
                </div>
                <div class="col-12">
                    <div class="mobile-menu"></div>
                </div>
            </div>
            <div class="offcanvas-menu">
                <span class="menu-close"><i class="fas fa-times"></i></span>
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Portfolio</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </div>
            <div class="offcanvas-overly"></div>
        </div>
    </div>
    </div>
</header>

@yield('content')

<!-- footer-area -->
<footer class="footer-section">
    <div class="container">
        <div class="footer-cta pt-80 pb-20">
            <div class="row">
                <div class="col-xl-4 col-md-4 mb-30">
                    <div class="single-cta">
                        <i class="fas fa-map-marker-alt"></i>
                        <div class="cta-text">
                            <h4>Adres</h4>
                            <span>Flinthorst 1, 7815 RJ Emmen  </span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 mb-30">
                    <div class="single-cta">
                        <i class="fas fa-phone"></i>
                        <div class="cta-text">
                            <h4>Bel ons</h4>
                            <span><a href="tel:06-44145340" style="color:#5a5a5a">06-44 14 53 40</a></span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 mb-30">
                    <div class="single-cta">
                        <i class="far fa-envelope-open"></i>
                        <div class="cta-text">
                            <h4>Mail ons</h4>
                            <span><a href="mailto:info@wsdev.nl" style="color:#5a5a5a">info@wsdev.nl</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <!-- <div class="footer-content pt-50 pb-30">
            <div class="footer-pattern">
                <img src="../img/footer/footer_map.png"  alt="">
            </div> -->
            <div class="row">

                <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                    <div class="footer-widget">
                        <div class="footer-widget-heading">
                            <h3>Aanbod</h3>
                        </div>
                        <ul>
                            <li><a href="#">Webontwikkeling</a></li>
                            <li><a href="#">Domeinnaam regestratie</a></li>
                            <li><a href="#">UI Design</a></li>
                            <li><a href="#">SEO optimalisatie</a></li>
                            <li><a href="#">Webdesign</a></li>
                            <li><a href="#">Webhosting</a></li>
                            <li><a href="#">CMS Systeem</a></li>
                            <li><a href="#">Copywriting</a></li>

                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                    <div class="footer-widget">
                        <div class="footer-widget-heading">
                            <h3>Social media</h3>
                        </div>
                        <ul>
                            <li><a href="https://www.linkedin.com/company/wsdev/" target="_blank">LinkedIN</a></li>
                            <li><a href="https://www.facebook.com/wsdev.nl/?modal=admin_todo_tour" target="_blank">Facebook</a></li>
                            <li><a href="https://www.instagram.com/wsdev.nl/?hl=nl" target="_blank">Instagram</a></li>
                            <li><a href="https://goo.gl/PikHch">Whatsapp</a></li><li></li>
                            <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Ffacebook.com%2Fwsdev.nl&width=150&layout=button&action=like&size=small&show_faces=true&share=true&height=65&appId" width="200" height="65" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                            

                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-50">
                    <div class="footer-widget">
                        <div class="footer-widget-heading">
                            <h3>Zakelijk</h3>
                        </div>
                        <ul>
                            <li><a href="https://bit.ly/2G6UMke">KVK: 72547626</a></li>
                            <li><a href="https://bit.ly/2G6UMke">Voorwaarden</a></li>
                            <li><a href="https://bit.ly/2G6UMke">BTW: 72547626</a></li>
                            <li><a href="https://bit.ly/2G6UMke">Privacy</a></li>
                            <p> Ontwikkeld met <img src="https://loading.io/spinners/heart/lg.beating-heart-preloader.gif" width="6%"> </a> in Drenthe </p>

                        </ul>
                    </div>
                </div>
                
                <!-- <div class="col-xl-4 col-lg-4 mb-50">
                    <div class="footer-widget">
                        
                        <div class="footer-social-icon">
                            <span>Volg ons</span>
                            <a href="https://goo.gl/xNmWVH" target="blank"><i class="fab fa-instagram instagram-bg"></i></a>
                            <a href="https://goo.gl/PikHch" target="blank"><i class="fab fa-whatsapp whatsapp-bg"></i></a>
                            <a href="https://goo.gl/qyn1rJ" target="blank"><i class="fab fa-linkedin-in linkedin-bg"></i></a>

                        </div>
                    </div>
                </div>  -->
            </div>
        </div>
    </div>
    
    <div class="copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 text-center text-lg-left">
                   <!--  <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fwsdev.nl%2F&width=74&layout=button_count&action=like&size=large&show_faces=true&share=false&height=21&appId" width="125" height="50" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe> -->
                    <div class="copyright-text">
                        <p>Copyright &copy; 2018, Alle rechten voorbehouden <a href="htts://wsdev.nl">WSDEV</a></p><br>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 d-none d-lg-block text-right">
                    <div class="footer-menu">
                        <ul> 
                            <!-- <li><a href="https://wsdev.nl/voorwaarden.pdf" download>Voorwaarden </a></li>
                            <li><a href="https://wsdev.nl/privacy.pdf" download>Privacy </a></li> -->
                            <li><a href="https://wsdev.nl">Website door WSDEV</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
@include('scripts')