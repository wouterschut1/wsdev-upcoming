@extends('layout')
@section('title')
Contact
@endsection
@section('menu')
     <div class="col-xl-9 col-lg-9 text-right">
        <div class="main-menu">
            <nav id="mobile-menu">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="/over-ons" >Over ons</a></li>
                    <li><a href="/services">Onze service</a></li>
                    <li><a href="/projecten">Portfolio</a></li>
                    <li><a href="/contact">Contact</a></li>
                    <li><a href="/dashboard">Portaal</a></li>
                    <!-- <li><a href="http://scrum.wsdev.nl" target="blank">Mijn WSDEV</a></li> -->
                </ul>
            </nav>
        </div>
    </div>
@endsection
@section('content')
<section class="breadcrumb-area pb-70 pt-100 grey-bg" style="background-image:url(img/bg/page-title.png)">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-md-6 mb-30">
                        <div class="breadcrumb-title sm-size">
                            <h2>contact </h2>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 text-left text-md-right mb-30">
                        <div class="breadcrumb">
                            <ul>
                                <li><a href="/home" style="color: white">Home</a></li>
                                <li><a href="" style="color: white">contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<section class="contact-area pb-70 pt-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-md-4 text-center text-md-right mb-30">
                        <div class="contact-person">
                           <h4>Anno nu</h4>
                            <p>Technologie gaat steeds verder en wordt het voor ons allen makkelijker gemaakt. Daarom kunt u ons gewoon een whatsapp bericht je sturen</p>
                            <a href="#" class=""><i class="far fa-calendar-alt"></i>Ma - Vri 9:00  - 19:00 </a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-4 text-center text-md-center mb-30">
                        <div class="contact-person">
                            <h4>Anno nu</h4>
                            <p>Technologie gaat steeds verder en wordt het voor ons allen makkelijker gemaakt. Daarom kunt u ons gewoon een whatsapp bericht je sturen</p>
                            <a href="#" class=""><i class="far fa-calendar-alt"></i>Ma - Vri 9:00  - 19:00 </a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-4 text-center text-md-left mb-30">
                        <div class="contact-person">
                             <h4>Anno nu</h4>
                            <p>Technologie gaat steeds verder en wordt het voor ons allen makkelijker gemaakt. Daarom kunt u ons gewoon een whatsapp bericht je sturen</p>
                             <a href="#" class=""><i class="far fa-calendar-alt"></i>Ma - Vri 9:00  - 19:00 </a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-4 text-center text-md-left mb-30">
                        <div class="contact-person">

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- contact-maps -->

        <div class="maps-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="maps">
                            <a href="https://www.google.com/maps/place/Flinthorst+1,+7815+RJ+Emmen/data=!4m2!3m1!1s0x47b7e638064cc963:0x32546bb7f5c771d9?ved=2ahUKEwi58fiiyrDfAhVpp1kKHea5DLIQ8gEwAHoECAAQAQ" target="_blank"><img src="img/map.png" class="img-fluid" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- contact-form -->
        <section class="pb-50 pt-50">
        </section>
    </main>

@endsection