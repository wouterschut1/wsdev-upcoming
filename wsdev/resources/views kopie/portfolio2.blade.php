@extends('layout')

@section('content')
   <!-- portfolio-details -->
   <section class="portfolio-details pt-100 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-45">
                        <div class="details-thumb">
                            <img src="img/portfolio/portfolio-details.jpg" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-4 mb-30 d-none d-lg-block">
                        <div class="project-status">
                            <div class="project-details-title">
                                <h3>Project Details</h3>
                                <ul>
                                    <li><b>Date</b><span>09 Oct 2018</span></li>
                                    <li><b>Location</b><span>New York, W2 3XE</span></li>
                                    <li><b>CLIENT</b><span>BD Company</span></li>
                                    <li><b>Category</b><span>Business</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-8 mb-30">
                        <div class="project-desc">
                            <h3>Project Description</h3>
                            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth the master-builder of human happiness. No one rejects dislikes or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain but because occasionaly circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Related Project -->
        <section class="related-project  pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-two">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb">
                                <img src="img/portfolio/3.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="/webontwikkeling">Webontwikkeling</a></h5>
                                <span>supporting text</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 d-lg-none d-md-block display-none">
                        <div class="project-status">
                            <div class="project-details-title">
                                <h3>Project Details</h3>
                                <ul>
                                    <li><b>Date</b><span>09 Oct 2018</span></li>
                                    <li><b>Location</b><span>New York, W2 3XE</span></li>
                                    <li><b>Category</b><span>Business</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-two">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb">
                                <img src="img/portfolio/6.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="#">Finance Consultancy</a></h5>
                                <span>supporting text</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 grid-item cat-two">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-thumb">
                                <img src="img/portfolio/4.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="portfolio-content">
                                <h5><a href="#">Finance Consultancy</a></h5>
                                <span>supporting text</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- clients-area -->
        <div class="clients-area grey-bg pt-50 pb-50">
            <div class="container">
                <div class="row">
                    <div class="clients-active owl-carousel">
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-1.png" alt="">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-2.png" alt="">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-3.png" alt="">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-4.png" alt="">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-5.png" alt="">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-6.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection