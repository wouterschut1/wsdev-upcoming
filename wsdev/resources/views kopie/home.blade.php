@extends('layout')
@section('title')
Home
@endsection
@section('menu')
     <div class="col-xl-9 col-lg-9 text-right">
        <div class="main-menu">
            <nav id="mobile-menu">
                <ul>
                    <li><a href="/" class="menu-active">Home</a></li>
                    <li><a href="/over-ons" >Over ons</a></li>
                    <li><a href="/services">Onze service</a></li>
                    <li><a href="/projecten">Portfolio</a></li>
                    <li><a href="/contact">Contact</a></li>
                    <li><a href="/dashboard">Portaal</a></li>
                    <!-- <li><a href="http://scrum.wsdev.nl" target="blank">Mijn WSDEV</a></li> -->
                </ul>
            </nav>
        </div>
    </div>
@endsection
@section('content')

    <main>
        <!-- slider-area -->
        <section class="slider-area">
            <div class="slider-active">
                <div class="single-slider slider-bg d-flex align-items-center" style="background-image:url(img/slider/slider2.jpg)">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="slider-content">
                                    <h1 data-animation="fadeInDown" data-delay=".2s">Wij bouwen <br> websites van<span> nu.</span></h1>
                                    <p data-animation="fadeInLeft" data-delay=".4s">Dit is WS Development! Een jong team van ontwikkelaars en design specialisten. <br>Wij realiseren uw online representatie</p>
                                    <a href="/" class="btn" data-animation="fadeInUp" data-delay=".8s">Bekijk ons aanbod</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-slider slider-bg d-flex align-items-center" style="background-image:url(img/slider/slider3.jpg)">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="slider-content">
                                    <h1 data-animation="fadeInUp" data-delay=".2s">Wij maken webdesign<br> bereikbaar voor <span>iedereen.</span></h1>
                                    <p data-animation="fadeInLeft" data-delay=".4s">There are many variations of passages of Lorem Ipsum available but the majority have sufered <br>alteration
                                        in some form by injected humour randomised</p>
                                    <a href="#" class="btn" data-animation="fadeInUp" data-delay=".6s">Bekijk ons aanbod</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="single-slider slider-bg d-flex align-items-center" style="background-image:url(img/slider/slider3.jpg)">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="slider-content">
                                    <h1 data-animation="fadeInUp" data-delay=".2s">Wij leveren complete UI designs aan voor<span> iedereen.</span></h1>
                                    <p data-animation="fadeInLeft" data-delay=".4s">There are many variations of passages of Lorem Ipsum available but the majority have sufered <br>alteration
                                        in some form by injected humour randomised</p>
                                    <a href="#" class="btn" data-animation="fadeInUp" data-delay=".6s">Ontdek de mogelijkheden</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </section>
        <!-- about-area -->
        <section class="about-area pb-60 pt-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 mb-40">
                        <div class="section-title">
                            <h2>Over ons</h2>
                        </div>
                        <div class="about-content">
                            <p>Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. <br> <br>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 mb-20 md-margin">
                        <div class="row">
                            <div class="col-xl-8 col-md-6 mb-20">
                                <div class="about-img">
                                    <img src="img/about/klein.png" class="img-fluid animated infinite bounce  slower" alt="" style="margin-left: 20px ">

                                </div>
                            </div>
                            <!-- <div class="col-xl-4 col-md-6 mb-20">
                                <div class="about-img">
                                    <img src="img/about/about4.jpg" class="img-fluid" alt=""><img src="img/about/about3.jpg" class="img-fluid" alt="">
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- service-area -->
        <section class="service-area grey-bg pb-70 pt-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center mb-40">
                        <div class="section-title service-title">
                            <h2>Onze service</h2>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,words which don't look even slightly believable.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 text-center mb-30">
                        <div class="features-wrap">
                            <div class="features-icon">
                                <span class="lnr lnr-code"></span>
                            </div>
                            <h4>Web ontwikkeling</h4>
                            <p>Et harum quidem rerum facilis est et expe dita dis tinctio. Nam libero tempore, cum soluta
                                nobis..</p>
                            <a href="/webontwikkeling">Meer informatie</a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 text-center mb-30">
                        <div class="features-wrap">
                            <div class="features-icon">
                                <span class="lnr lnr-layers"></span>
                            </div>
                            <h4>UI Design</h4>
                            <p>Et harum quidem rerum facilis est et expe dita dis tinctio. Nam libero tempore, cum soluta
                                nobis..</p>
                            <a href="/webdesign">Meer informatie</a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 text-center mb-30">
                        <div class="features-wrap">
                            <div class="features-icon">
                               <span class="lnr lnr-text-align-center"></span>
                            </div>
                            <h4>Content creation</h4>
                            <p>Et harum quidem rerum facilis est et expe dita dis tinctio. Nam libero tempore, cum soluta
                                nobis..</p>
                            <a href="/contentcreation">Meer informatie </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 text-center mb-30">
                        <div class="features-wrap">
                            <div class="features-icon">
                                <span class="lnr lnr-pencil"></span>
                            </div>
                            <h4>Domeinnaam registratie</h4>
                            <p>Et harum quidem rerum facilis est et expe dita dis tinctio. Nam libero tempore, cum soluta
                                nobis..</p>
                            <a href="/hosting">Meer informatie</a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 text-center mb-30">
                        <div class="features-wrap">
                            <div class="features-icon">
                                <span class="lnr lnr-database"></span>
                            </div>
                            <h4>Webhosting</h4>
                            <p>Et harum quidem rerum facilis est et expe dita dis tinctio. Nam libero tempore, cum soluta
                                nobis..</p>
                            <a href="/hosting">Meer informatie</a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 text-center mb-30">
                        <div class="features-wrap">
                            <div class="features-icon">
                                <span class="lnr lnr-thumbs-up"></span>
                            </div>
                            <h4>Toegewijde ondersteuning</h4>
                            <p>Et harum quidem rerum facilis est et expe dita dis tinctio. Nam libero tempore, cum soluta
                                nobis..</p>
                            <!-- <a href="">Meer informatie</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- team-are -->
        <section class="team-area grey-bg pt-100 pb-70">
            <div class="container">
                <div class="row">

                </div>
            </div>
        </section>
        <!-- cta-area -->
        <section class="cta-area" style="background-image:url(img/bg/cta1.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center">
                        <div class="call-to-action white-text">
                            <h3>Als klant krijgt u toegang tot uw persoonlijke dashboard met voortgang van het project</h3>
                            <div class="call-to-action">
                                <a href="https://wsdev.nl/scrum" class="btn video-view">Mijn dashboard</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- blog-area -->
        <section class="blog-area grey-bg pt-100 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center pb-40">
                        <div class="section-title service-title">
                            <h2>Laatste berichten</h2>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                                alteration in some form,words which don't look even slightly believable.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="https://www.linkedin.com/feed/update/urn:li:activity:6473840212081541120" target="blank">
                                    <img src="img/blog/I-01.jpg" alt="WSDEV brainstrom nieuwe activiteiten" />
                                </a>
                            </div>
                            <div class="blog-content home-blog">
                                <h2 class="blog-title">
                                    <a href="https://www.linkedin.com/feed/update/urn:li:activity:6473840212081541120" target="blank">Nieuwe uitdaging voor WSDEV</a>
                                </h2>
                                <p>Een paar brainstormsessies heeft tot een nieuwe, toffe samenwerking geleid tussen Rene de Groot (Nobanovi) & WSDEV. Samen werken wij aan een product die wij in de eerste helft van 2019 zullen lanceren.</p>
                            </div>
                            <div class="link-box home-blog-link">
                                <a href="https://www.linkedin.com/feed/update/urn:li:activity:6473840212081541120" target="blank">Bekijk artikel</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="https://www.linkedin.com/feed/update/urn:li:activity:6443128346112069632" target="blank">
                                    <img src="img/blog/I-02.jpg" alt="WSDEV kom in gesprek voor webdevelopment in Emmen" />
                                </a>
                            </div>
                            <div class="blog-content home-blog">
                                <h2 class="blog-title">
                                    <a href="https://www.linkedin.com/feed/update/urn:li:activity:6443128346112069632" target="blank">Kom in gesprek met ons team!</a>
                                </h2>
                                <p>De beste ideeën komen op met cappuccino! Kom langs en kom in gesprek! </p>
                            </div>
                            <div class="link-box home-blog-link">
                                <a href="https://www.linkedin.com/feed/update/urn:li:activity:6443128346112069632" target="blank">Bekijk artikel</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                        <div class="blog-wrapper home-blog-wrapper white-bg">
                            <div class="blog-thumb">
                                <a href="https://www.linkedin.com/feed/update/urn:li:activity:6443128346112069632" target="blank">
                                    <img src="img/blog/I-03.jpg" alt="" />
                                </a>
                            </div>
                            <div class="blog-content home-blog">
                                <h2 class="blog-title">
                                    <a href="https://www.linkedin.com/feed/update/urn:li:activity:6443128346112069632" target="blank">WSDEV is gezond! Ren jij ook mee?</a>
                                </h2>
                                <p>Op zondag 16 september loopt WSDEV mee aan de 10 km run van Parc Sandur! Doe jij ook mee?  https://wsdev.nl</p>
                            </div>
                            <div class="link-box home-blog-link">
                                <a href="https://www.linkedin.com/feed/update/urn:li:activity:6443128346112069632" target="blank">Bekijk artikel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
 <!-- clients-area -->

        <div class="clients-area pt-50 pb-50">
        <div class="row">
                    <div class="col-xl-12 text-center mb-40">
                        <div class="section-title service-title">
                            <h2>Wij werken met</h2>
                            <p>WSDEV werkt met verschillende software en partners om alle diensten en producten zo optimaal mogelijk te kunnen leveren. <br> De software en partners die voor ons een grote rol speelt zie je hier onder afgebeeld.</p>
                        </div>
                    </div>
                </div>
            <div class="container">
                <div class="row">
                    <div class="clients-active owl-carousel">
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-1.png" widht="20%" alt="">
                               <!-- <p> <a href="https://metador.nl">Metador Pro </a><br> <hr> CMS & CRM Systeem</p> -->

                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-2.png"widht="20%" alt="">
                                <!-- <p> <a href="https://metador.nl">Adobe XD </a><br> <hr> Desing tool</p> -->
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-3.png"widht="20%" alt="">
                                <!-- <p> <a href="https://metador.nl">JetBrains </a><br> <hr> Software ontwikkel tool</p> -->
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-4.png"widht="20%" alt="">
                                <!-- <p> <a href="https://metador.nl">JetBrains </a><br> <hr> Software ontwikkel tool</p> -->
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-05.png"widht="20%" alt="">
                                <!-- <p> <a href="https://metador.nl">JetBrains </a><br> <hr> Software ontwikkel tool</p> -->
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-06.png"widht="20%" alt="">
                                <!-- <p> <a href="https://metador.nl">JetBrains </a><br> <hr> Software ontwikkel tool</p> -->
                            </div>
                        </div>
                          <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-07.png"widht="20%" alt="">
                                <!-- <p> <a href="https://metador.nl">JetBrains </a><br> <hr> Software ontwikkel tool</p> -->
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-clients">
                                <img src="img/clients/client-08.png"widht="20%" alt="">
                                <!--<a href="https://metador.nl">JetBrains </a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
<span>
 <section class="service-area grey-bg pb-70 pt-20"></section>

@endsection

