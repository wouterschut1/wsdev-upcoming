<!-- Include alle styles -->
        <link rel="stylesheet" href="{{ asset('/styles/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/styles/fontawesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/styles/icofont.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/styles/linearicons.css') }}">
        <link rel="stylesheet" href="{{ asset('/styles/slick.css') }}">
        <link rel="stylesheet" href="{{ asset('/styles/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('/styles/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/styles/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('/styles/meanmenu.css') }}">
        <link rel="stylesheet" href="{{ asset('/styles/default.css') }}">
        <link rel="stylesheet" href="{{ asset('/styles/style.css') }}">
        <link rel="stylesheet" href="{{ asset('/styles/responsive.css') }}">
        <link rel="stylesheet" href="{{ asset('/styles/main.css') }}">
        <link rel="stylesheet" href="{{ asset('/styles/theme/pages.css') }}">
        @yield('additional-styles')
        
<!-- ./ Styles -->