<?php

// Alle views die in deze applicatie worden gebruikt zijn te vinden in path: wsdev > resources > views


//-----------------------------------

 // Home pages     //

Route::get('/', function () {                                           //home
    return view('home');
});
Route::get('/home', function () {                                       //home
    return view('home');
});

//-----------------------------------

 // About page   //

Route::get('/over-ons', function () {                                   //over ons
    return view('about');
});

//-----------------------------------

 // services pages //

Route::get('/services', function () {                                   //services
    return view('services');
});
Route::get('/services-details', function () {                           //service details
    return view('services2');
});

//-----------------------------------


 // All pages for services tabs in services details:

Route::get('/webontwikkeling', function () {       // webontwikkeling    [service onderdeel]
    return view('webontwikkeling');
});
Route::get('/webdesign', function () {             // webdesing          [service onderdeel]
    return view('webdesign');
});
Route::get('/hosting', function () {               // hosting            [service onderdeel]
    return view('hosting');
});
Route::get('/contentcreation', function () {       //contentcreation     [service onderdeel]
    return view('contentcreation');
});
Route::get('/seo', function () {                   //SEO                 [service onderdeel]
    return view('seo');
});




//-----------------------------------


 // project pages //

Route::get('/projecten', function () {                               //projecten
    return view('portfolio');
});
Route::get('/project-details', function () {                         // project details
    return view('portfolio2');
});


//-----------------------------------


// All pages for services tabs in projects details:

Route::get('/project-details/schutconsultancy', function () {       // webontwikkeling    [project onderdeel]
    return view('schutconsultancy');
});
Route::get('/project-details/worldrun', function () {              // webdesing           [project onderdeel]
    return view('worldrun');
});


//-----------------------------------

 // contact pages //
Route::get('/contact', function () {                                 // contact
    return view('contact');
});


//-----------------------------------


 // Dashboard pagina's //
 Route::get('/dashboard', function () {                                 // dashboard
    return view('dashboard');
});


//-----------------------------------



